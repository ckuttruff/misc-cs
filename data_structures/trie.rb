require 'benchmark'

class Trie
  attr_accessor :terminal
  attr_reader :children, :root
  def initialize(root_node = self)
    @root = root_node
    @children = {}
    @terminal = false
  end

  def insert(word)
    node = @root
    word.each_char do |char|
      node.children[char] = Trie.new(@root) unless node.children[char]
      node = node.children[char]
    end
    node.terminal = true
  end

  def find(word)
    curr_node = root
    word.each_char do |char|
      next_node = curr_node.children[char]
      return false if next_node.nil?
      curr_node = next_node
    end
    true
  end
end

root = Trie.new
File.readlines("words.txt").each do |line|
  root.insert(line.strip)
end

# once trie initialized, ~3 orders of magnitude faster than grep for lookup
%w(aardvard enthusiast nerd zoo).each do |word|
  res = false;
  time = Benchmark.measure { res = root.find(word) }
  puts "#{time} - Found #{word}" if res
end

