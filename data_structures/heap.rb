class Heap

  # will us a max heap
  attr_accessor :heap
  def initialize(arr)
    # keep first element of heap array empty to simplify math
    @heap = arr
    (@heap.length / 2).downto(0).each{|idx| heapify(idx) }
  end

  # -------------------------------------------------------------------
  # Key operations for heap
  def extract_max
    val = @heap.first
    val_last = @heap.pop
    @heap[0] = val_last if @heap.length > 0
    heapify(0)
    val
  end

  def insert(elem)
    idx = @heap.length
    p_idx = parent(idx)
    @heap[idx], pval = elem, @heap[p_idx]
    while(idx > 0 && pval && elem > pval)
      swap(idx, p_idx)
      idx, p_idx = p_idx, parent(p_idx)
      pval = @heap[p_idx]
    end
  end

  def heapify(idx)
    return if leaf?(idx)
    c = max_child(idx)
    v, cv = @heap[idx], @heap[c]
    return if v > cv
    swap(idx, c)
    heapify(c)
  end

  def max_child(idx)
    i1, i2 = l_child(idx), r_child(idx)
    v1, v2 = @heap[i1], @heap[i2]
    return nil if v1.nil?
    return i1  if v2.nil?
    (v1 > v2) ? i1 : i2
  end

  def swap(idx_1, idx_2)
    @heap[idx_1], @heap[idx_2] = @heap[idx_2], @heap[idx_1]
  end

  # -------------------------------------------------------------------
  # Accessing nodes
  def parent(idx)
    (idx - 1) / 2
  end

  def l_child(idx)
    (idx * 2) + 1
  end

  def r_child(idx)
    (idx * 2) + 2
  end

  def leaf?(idx)
    l_child(idx) >= @heap.length
  end

end

arr = (1..5_000_000).to_a
h = Heap.new(arr)
res = []
while(v = h.extract_max)
  res << v
end
puts res.first
puts res.last
