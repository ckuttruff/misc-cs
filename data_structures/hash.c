#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STR_SIZE 256
#define TABLE_SIZE   5
#define true  1;
#define false 0;

typedef struct {
  char * name;
  int age;
  struct person * next;
} person;

person * hash_table[TABLE_SIZE];

void table_init(void) {
  for(int i = 0; i < TABLE_SIZE; ++i)
    hash_table[i] = NULL;
}

long hash(char * str) {
  long res;
  for(unsigned int i = 0; i < strnlen(str, MAX_STR_SIZE); ++i)
    res = (res * str[i] + str[i]) % TABLE_SIZE;
  return res;
}

person * table_find(char * name) {
  long idx = hash(name);
  person * curr_node = hash_table[idx];

  while(curr_node != NULL) {
    if(strncmp(curr_node->name, name, MAX_STR_SIZE) == 0)
      return curr_node;
    else
      curr_node = curr_node->next;
  }
  return NULL;
}

void table_insert(person * p) {
  long idx = hash(p->name);
  person * target = hash_table[idx];
  person * prev   = target;

  if(target == NULL)
    hash_table[idx] = p;
  else {
    while(target != NULL) {
      prev   = target;
      target = target->next;
    }
    prev->next = p;
  }
}

void table_display(void) {
  printf("-------------------------------------------------------------\n");
  for(int i = 0; i < TABLE_SIZE; ++i)
    if(hash_table[i] == NULL)
      printf("%d: _______\n", i);
    else {
      person * curr_node = hash_table[i];
      while(curr_node != NULL) {
	printf("%d: %s (age: %d)\t|\t", i, curr_node->name, curr_node->age);
	curr_node = curr_node->next;
      }
      printf("\n");
    }
}

int main(void) {
  table_init();
  table_display();

  person arr[] = {
    {.name = "Chris", .age = 33}, {.name = "Sean",  .age = 30},
    {.name = "Bob",   .age = 44}, {.name = "Alice", .age = 12},
    {.name = "Foo",   .age = 19}, {.name = "Bar",   .age = 17},
    {.name = "Argh",  .age = 4},  {.name = "Gah",   .age = 99}};
  int arr_size = 8;

  for(int i = 0; i < arr_size; ++i)
    table_insert(&arr[i]);
  table_display();

  for(int i = 0; i < arr_size; ++i) {
    person * p = table_find(arr[i].name);
    if(p == NULL)
      printf("%s NOT FOUND\n", arr[i].name);
    else
      printf("%s FOUND!!\n", arr[i].name);
  }

  printf("Searching Foo: %p\n", table_find("Foo"));
  printf("Searching Baz: %p\n", table_find("Baz"));

  return 0;
}
