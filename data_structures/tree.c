#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 10000000

typedef struct Tree {
  long value;
  struct Tree * left;
  struct Tree * right;
} Tree;

Tree * insert(Tree*, Tree*);
Tree * search(Tree*, long);
void init_node(Tree*, long);

int main() {
  int i;
  Tree root, bar;
  init_node(&root, 500);
  init_node(&bar, 9193);

  Tree * t = (Tree *) malloc(MAX_SIZE * sizeof(Tree));
  for(i = 0; i < MAX_SIZE; ++i) {
    Tree * n = t + i;
    n->value = rand();
    n->left  = NULL;
    n->right = NULL;
    insert(&root, n);
  }
  insert(&root, &bar);

  printf("root: %p\n", &root);
  printf("search(-2): %p\n", search(&root, -2));
  printf("search(9193): %p\n", search(&root, 9193));

  return 0;
}

// Proper initialization of tree with pointers set to NULL
void init_node(Tree* n, long value) {
  n->value = value;
  n->left  = NULL;
  n->right = NULL;
}

Tree * insert(Tree * node, Tree * node2) {
  if(node2->value <= node->value)
    (node->left == NULL) ? node->left = node2 :
      insert(node->left, node2);
  else
    (node->right == NULL) ? node->right = node2 :
      insert(node->right, node2);
  return node2;
}

Tree * search(Tree * node, long val) {
  if(node == NULL)
    return NULL;
  if(node->value == val)
    return node;
  (val < node->value) ? search(node->left, val) :
    search(node->right, val);
}
