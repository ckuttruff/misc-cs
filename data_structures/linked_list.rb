class LinkedList

  class Node
    attr_accessor :data, :next_node
    def initialize(data, next_node = nil)
      @data = data
      @next_node = next_node
    end
  end

  attr_reader :head, :size
  def initialize(arr)
    @size = 0
    @head = nil
    arr.each { |elem| insert(elem) }
  end

  def insert(elem)
    @head = Node.new(elem, @head)
    @size += 1
  end

  def each(&block)
    node = @head
    while(node)
      block.call(node)
      node = node.next_node
    end
  end

end
