#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_WORD_SIZE 25
#define MAX_CHARS 256

typedef struct trie_node {
  struct trie_node * children[MAX_CHARS];
} trie_node;


// obv very inefficient, but a naive v1 implementation
//   still reads in a dictionary file in less then 0.1s
trie_node * create() {
  trie_node * new = (trie_node *) malloc(sizeof(trie_node));
  for(int i = 0; i < MAX_CHARS; ++i)
    new->children[i] = NULL;
  return new;
}

void print_trie(trie_node * root) {
  trie_node * curr = root;
  int stack[MAX_CHARS];

  for(int i = 0; i < MAX_CHARS; ++i) {
    if(curr->children[i] != NULL) {
      printf("%c - ", i);
      print_trie(curr->children[i]);
      printf("\n");
    }
  }
}

bool insert(trie_node * root, char * text) {
  trie_node * curr = root;
  int length = strlen(text);

  for(int i = 0; i < length; ++i) {
    char c = text[i];
    if(curr->children[c] == NULL)
      curr->children[c] = create();
    insert(curr->children[c], text + 1);
    break;
  }
}

int main(void) {
  trie_node * root = create();
  FILE * word_file = fopen("words.txt", "r");
  // more than enough any for anything in words.txt
  int max_size;
  char word[MAX_WORD_SIZE];

  while(fgets(word, MAX_WORD_SIZE, word_file)) {
    int len = strlen(word) - 1;
    word[len] = '\0';
    insert(root, word);
  }
  fclose(word_file);
  //print_trie(root);

  return 0;
}
