#include <stdio.h>
#include <stdlib.h>

typedef struct node {
  int value;
  struct node * next;
} node;

node * create_node(int val) {
  node * n = (node *) malloc(sizeof(node));
  n->value = val;
  n->next  = NULL;
  return n;
}

// free this
void insert(node * n, int val) {
  node * new = create_node(val);
  (n->next == NULL) ? n->next = new :
    insert(n->next, val);
}

// pass double pointer to adjust reference in case of removing head
void remove_node(node ** head, int val) {
  node * n1 = *head;
  node * n2 = n1->next;

  if(n1->value == val) {
    node * tmp = *head;
    *head = n1->next;
    free(tmp);
  } else {
    while(n2 != NULL) {
      if(n2->value == val) {
	n1->next = n2->next;
	free(n2);
	return;
      }
      n1 = n2;
      n2 = n2->next;
    }
  }
}

void print_list(node * head) {
  node * curr = head;
  while(curr != NULL) {
    printf("(addr: %p) - (val: %d) -> ", curr, curr->value);
    curr = curr->next;
  }
  printf("(nil) \n");
}

int main(void) {
  node * head = create_node(1);
  insert(head, 2);
  insert(head, 3);
  insert(head, 4);

  print_list(head);
  printf("Removing 3\n");
  remove_node(&head, 3);
  print_list(head);
  printf("Removing head\n");
  remove_node(&head, 1);
  print_list(head);

  return 0;
}
