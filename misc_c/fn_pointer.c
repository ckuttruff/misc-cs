#include <stdio.h>
#include <stdlib.h>

int * map_int(int * arr, int arr_size, int (*fn) (int)) {
  int * new_arr = (int *) malloc(arr_size * sizeof(int));
  for(int i = 0; i < arr_size; ++i)
    new_arr[i] = fn(arr[i]);
  return new_arr;
}

int sqr(int n) {
  return n * n;
}

int main(void) {
  int * res;
  int arr[] = {1, 2, 3, 4, 5};

  res = map_int(arr, 5, sqr);

  printf("-------------------------------------------\n");
  printf("orig arr: ");
  for(int i = 0; i < 5; ++i)
    printf("%d, ", arr[i]);
  printf("\n");

  printf("-------------------------------------------\n");
  printf("res arr: ");
  for(int i = 0; i < 5; ++i)
    printf("%d, ", res[i]);
  printf("\n");
}
