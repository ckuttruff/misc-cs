@run_count = 0

def fact(n)
  # show each call to our function nested with spaces
  @run_count += 1
  puts "#{'  ' * @run_count}fact(#{n})"

  return 1 if(n == 1)
  res = n * fact(n - 1)

  # show the resultant value
  @run_count -= 1
  puts "#{'  ' * @run_count}#{res})"
  return res
end

fact(10)
