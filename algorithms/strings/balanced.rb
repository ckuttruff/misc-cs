# Using a stack to check whether a string containing paren/brace/bracket is balanced

class String
  # assumes only getting passed some combination of just brackets/braces
  def balanced?
    stack = []
    h = { '[' => ']', '{' => '}', '(' => ')' }
    left, right = h.keys, h.values

    self.each_char do |char|
      if(left.include?(char))
        stack.push(char)
      elsif(right.include?(char))
        left_char = stack.pop
        # check if top of stack matches with current closing char
        return false unless h[left_char] == char
      end
    end

    stack.empty?
  end
end

[ # invalid
  '{', '[', '(', ')', '}', ']', '()[', '([))',
  # valid
  '()', '(())', '(foo bar)', '(this (looks like) (Lisp))', '[({[]})]'].each do |str|

  puts "balanced? (#{str.balanced?}) - #{str}"
end
