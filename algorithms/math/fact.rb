def fact_basic(n)
  return 1 if(n == 1)
  n * fact(n - 1)
end

def fact_tco(n, acc = 1)
  return acc if n == 1
  fact_tco(n - 1, acc * n)
end

def fact_loop(n)
  res = 1
  n.downto(1).each { |n1| res *= n1 }
  res
end

def fact_func(n)
  (1..n).reduce(:*)
end

