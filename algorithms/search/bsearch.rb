$counter = 0
class Array
  # returns index of array if element is found; else nil
  def bsearch(val, low = 0, high = self.size)
    $counter += 1
    return nil if low > high
    mid = (low + high) / 2
    return mid if self[mid] == val
    return bsearch(val, low, mid) if(val < self[mid])
    return bsearch(val, (mid + 1), high)
  end

end

# hooray for log(n) time :)
arr = (1..100_000_000).to_a
puts arr.bsearch(rand(100_000_000))
puts "steps: #{$counter}"
