#include <stdio.h>
#include <stdlib.h>

long binsearch (long *, long, long, long);

int main() {
  long i, idx, val;
  long max_size = 100000000; // 100 million
  long * arr = malloc(max_size * sizeof(long));

  // initialize sorted array
  for(i = 0; i < max_size; ++i)
    arr[i] = i;

  val = (long) rand % max_size;
  idx = binsearch(arr, val, 0, max_size - 1);
  printf("val(%ld), idx (%ld)\n", val, idx);

  return 0;
}

// returns index of arr if found, else -1
long binsearch(long * arr, long val, long low, long high) {
  printf("binsearch: val(%ld), low(%ld), high(%ld)\n", val, low, high);
  long mid = (low + high) / 2;
  if(low > high)
    return -1;
  if(val == arr[mid])
    return mid;
  return (val < arr[mid]) ? binsearch(arr, val, low, mid) :
    binsearch(arr, val, (mid + 1), high);
}
