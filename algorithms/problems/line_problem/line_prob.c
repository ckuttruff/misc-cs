#include <stdio.h>

#define MAX_LINES 100

// returns index of shortest line
int shortest_line(int * lines, int num_lines) {
  int i;
  int shortest_idx = 0;

  for(i = 0; i < num_lines; ++i)
    if(lines[i] < lines[shortest_idx])
      shortest_idx = i;
  return shortest_idx;
}

void solve(int * lines, int num_lines, int people) {
  int i, j;
  for(i = 0; i < people; ++i) {
    j = shortest_line(lines, num_lines);
    printf("%d\n", lines[j]);
    lines[j]++;
  }
}

int main(void) {
  int i, num_lines, num_people;
  int lines[MAX_LINES];

  scanf("%d %d", &num_lines, &num_people);
  for(i = 0; i < num_lines; ++i)
    scanf("%d", &lines[i]);

  solve(lines, num_lines, num_people);

  return 0;
}
