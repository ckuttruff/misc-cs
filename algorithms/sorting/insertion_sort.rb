#  -----------------------------------------------------
#  * start with an unsorted array, and an empty array
#  * for each element of unsorted array, insert into correct position of new array

class InsertionSort
  def self.sort(arr)
    return [] if arr.empty?
    res = [arr.first]

    arr[1..].each do |n|
      max_idx = res.length - 1
      0.upto(max_idx) do |idx|
        if(n <= res[idx])
          res.insert(idx, n)
          break
        elsif(idx == max_idx)
          res.insert(idx + 1, n)
          break
        end
      end
    end
    res
  end
end
