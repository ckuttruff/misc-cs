require_relative 'insertion_sort'
require_relative 'merge_sort'
require 'test/unit'

class SortTest < Test::Unit::TestCase
  ARR = (1..50).to_a.shuffle

  %w(insertion merge).each do |type|
    define_method "test_#{type}_sort" do
      klass = Kernel.const_get("#{type.capitalize}Sort")
      res = klass.sort(ARR)
      log(type, res)
      assert(res == ARR.sort)
    end
  end

  private

  def log(type, res)
    puts '=================================================================='
    puts "#{type.capitalize} Sort!"
    puts '-------------'
    puts ARR.inspect
    puts "expected: #{ARR.sort.inspect}"
    puts "result:   #{res.inspect}"
  end
end
