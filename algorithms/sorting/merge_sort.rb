class MergeSort

  # recursively splits array into individual slices;
  #   merges sorted slices together on the way back up the call stack
  def self.sort(arr)
    return arr if arr.length <= 1
    mid_idx = arr.length / 2
    l, r = arr[0,mid_idx], arr[mid_idx..]
    merge(sort(l), sort(r))
  end

  def self.merge(l, r)
    res = []
    until(l.empty? && r.empty?)
      n1, n2 = l.first, r.first
      if(n1.nil? || n2 && n1 > n2)
        res << r.shift
      else
        res << l.shift
      end
    end
    res
  end

end
