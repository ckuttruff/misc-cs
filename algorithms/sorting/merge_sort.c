#include <stdlib.h>
#include <stdio.h>

#define ARR_SIZE 500000

void merge_sort(long*, long, long);
void merge(long*, long, long, long);

int main(void) {
  long i, j;
  long last_index = ARR_SIZE - 1;
  long arr[ARR_SIZE];

  // initialize our array
  for(i = 0, j = last_index; i <= last_index; ++i, --j) {
    arr[i] = j;
  }

  merge_sort(arr, 0, last_index);
  printf("first: %ld; mid: %ld; last: %ld\n", arr[0], arr[ARR_SIZE / 2], arr[last_index]);

  return 0;
}

void merge_sort(long * arr, long start, long end) {
  if(start < end) {
    long mid = (start + end) / 2;
    merge_sort(arr, start, mid);
    merge_sort(arr, mid + 1, end);
    merge(arr, start, mid, end);
  }
}

// merge is now correct
void merge(long * arr, long start, long mid, long end) {
  long size = (end - start) + 1;
  long temp[size];
  long i = start, j = (mid + 1), k = 0;

  while(i <= mid || j <= end)
    if(j > end || arr[i] <= arr[j]) {
      temp[k] = arr[i];
      ++k; ++i;
    } else {
      temp[k] = arr[j];
      ++k; ++j;
    }

  for(i = start; i <= end; ++i)
    arr[i] = temp[i - start];
}
